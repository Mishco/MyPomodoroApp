// Modules to control application life and create native browser window
const {
    app,
    BrowserWindow,
    Tray,
    Menu
} = require('electron')

var windowManager = require('electron-window-manager');

const join = require('path').join;

// Replace '..' with 'about-window'
const openAboutWindow = require('about-window').default;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
let tray = null

function createWindow() {
    tray = new Tray("clock-icon.png")
    const contextMenu = Menu.buildFromTemplate([{
            label: 'Settings',
            type: 'normal'
        },
        {
            label: 'About',
            type: 'normal'
        },
        {
            label: 'Item3',
            type: 'separator',
            checked: true
        },
        {
            label: 'Exit',
            type: 'normal'
        }
    ])
    tray.setToolTip('My Pomodoro')
    tray.setContextMenu(contextMenu)

    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 400,
        height: 450,
        icon: 'clock-icon.png',
        resizable: false,
    })

    // and load the index.html of the app.
    mainWindow.loadFile('index.html')

    // Open the DevTools.
    // mainWindow.webContents.openDevTools()

    const template = [{
            label: 'File',
            submenu: [{
                    type: 'separator'
                }, {
                    label: 'Settings',
                    click: function() {
                        // Create a new window
                        var homeWindow = windowManager.createNew('home', 'Welcome ...', 'prefs.html', false, {
                            'width': 450,
                            'height': 450,
                            'position': 'left',
                            'frame': true,
                            'transparent': false,
                            'showDevTools': false,
                            'resizable': true
                        }).open();
                        // windowManager.setDefaultSetup({ 'width': 450, 'height': 450, 'position': 'right', 'frame': true, 'transparent': true });
                        // windowManager.open('prefs', 'Welcome ...', '/prefs.html');
                    }
                },
                {
                    label: 'Exit',
                    click: () => {
                        app.quit();
                    }
                }

            ]
        },
        {
            label: 'Edit',
            submenu: [{
                    role: 'undo'
                },
                {
                    role: 'redo'
                },
                {
                    type: 'separator'
                },
                {
                    role: 'cut'
                },
                {
                    role: 'copy'
                },
                {
                    role: 'paste'
                },
                {
                    role: 'pasteandmatchstyle'
                },
                {
                    role: 'delete'
                },
                {
                    role: 'selectall'
                }
            ]
        },

        {
            label: 'View',
            submenu: [{
                    role: 'reload'
                },
                {
                    role: 'forcereload'
                },
                {
                    role: 'toggledevtools'
                },
                {
                    type: 'separator'
                },
                {
                    role: 'resetzoom'
                },
                {
                    role: 'zoomin'
                },
                {
                    role: 'zoomout'
                },
                {
                    type: 'separator'
                },
                {
                    role: 'togglefullscreen'
                }
            ]
        },
        {
            role: 'window',
            submenu: [{
                    role: 'minimize'
                },
                {
                    role: 'close'
                }
            ]
        },
        {
            role: 'help',
            submenu: [{
                label: 'About',
                click: () => openAboutWindow({
                    icon_path: join(__dirname, 'img/Working_Gears_Machine.png'),
                    copyright: 'Copyright (c) 2018 Mishco',
                    package_json_dir: __dirname,
                    // other information are from package.json
                }),
            }]
        }
    ]
    const menu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(menu)

    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.

        mainWindow = null
    })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function() {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function() {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.