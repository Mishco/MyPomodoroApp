var moment = require('moment');
var duration = require('moment-duration-format');

function startWorking() {
    // in seconds

    //console.log(parseInt(Number(document.getElementById("myRange").value) * 60));
    var timeInMinute = 1500; //parseInt(Number(document.getElementById("myRange").value) * 60); // 25min * 60
    while (timeInMinute > 0) {
        var seconds = --timeInMinute;
        var duration = moment.duration(seconds, 'seconds');
        var formatted = duration.format("mm:ss");
        document.getElementById("pomodoroLabel").innerHTML = formatted;
    }
}

function endWorking() {
    var pomodoroLabel = document.getElementsByTagName("pomodoroLabel");
    document.getElementById("pomodoroLabel").innerHTML = "END:00";
}

function myFunction() {
    document.getElementById("pomodoroLabel").innerHTML = "25:00";
}